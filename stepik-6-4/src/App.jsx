import './App.css';
import React from "react";
import CommentForm from './components/CommentForm';
import CommentList from './components/CommentsList';
import Comment from './components/Comment';
import { useEffect, useState } from 'react'; 
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

function App() {
  let [comments, setComments] = useState([]);
  let [userComments, setUserComments] = useState([]);

  let addToComments = (values) => {
    let newValues = values.map(e => <Comment values={e} key={comments.length + 1}/>);
    setComments(newValues.concat(comments))
  }

  let handleUserPost = (values) => {
    let newValues = values.map(e => <Comment values={e} key={comments.length + 1}/>);
    console.log(userComments)
    setUserComments(newValues.concat(userComments))
  }

  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/comments")
      .then(res => {
        console.log(res.data)
        let newValues = res.data.map(e => <Comment values={e} key={uuidv4()}/>);

        setComments(newValues)
        // addToComments(res.data);
    });
  }, [userComments])

  return (
    <main>
      <div className="form-wrapper">
        Super formik!
        <CommentForm updateCommentsCallback={handleUserPost}/>
      </div>
      <CommentList comments={comments}/>
    </main>
  )
}

export default App
