
export default function Comment({ values }) {
    return (
        <div className="comment">
            <b>{ values.name }</b>
            <i>{ values.email }</i>
            <p>{ values.body}</p>
        </div>
    )
}