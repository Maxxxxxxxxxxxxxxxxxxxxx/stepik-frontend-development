import React from 'react';
import { useState, useEffect } from 'react';
import { Form, Field, Formik } from 'formik';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

const EMAIL_RE = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

export default function CommentForm({ updateCommentsCallback }) {
  let validate = (values) => {
    if (values.name.length <= 1 || values.name.length >= 20) { alert("Niepoprawna nazwa! Musi być od 1 do 20 znaków"); return false; }
    else if (!EMAIL_RE.test(values.email)) { alert("Niepoprawny email!"); return false; }
    else return true;
  };

  return (
    <Formik
      initialValues={{
        name: '',
        email: '',
        body: ''
      }}
      onSubmit={(values) => {
        if (validate(values)) {
          console.log(values);

          let postData = {
            name: values.name,
            email: values.email,
            id: uuidv4(),
            postId: Math.floor(Math.random() * 100)
          };

          console.log(postData)

          axios.post("https://jsonplaceholder.typicode.com/comments", postData)
            .then(r => {
              // console.log(r.status)
              // console.log(r)
              if (r.status == 201) { 
                document.getElementById("formik").reset(); 
                updateCommentsCallback([values]) 
              }
            })
        } else {
          alert("Wystąpił błąd podczas walidacji!");
        }
      }}
    >
      <div className="wrapper">
        <Form className='comment-form' id="formik">
          <Field type="text" name="name" placeholder="name"></Field>
          <Field type="email" name="email" placeholder="email"></Field>
          <Field type="text" as='textarea' name="body" className="textarea" placeholder="Write something..."></Field>
          <button type="submit"> Submit </button>
        </Form>
        <button onClick={() => document.getElementById("formik").reset()}>Reset</button>
      </div>
    </Formik>
  )
}