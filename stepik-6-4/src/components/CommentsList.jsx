import React from 'react';

export default function CommentList({ comments }) {
    return (
        <ul className="list">
            {comments}
        </ul>
    )
}